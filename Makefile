all: player ringmaster
ringmaster: potato.o ringmaster.c potato.h
	gcc -Wall -std=gnu99 ringmaster.c potato.o -o ringmaster
player: potato.o player.c potato.h
	gcc -Wall -std=gnu99 player.c potato.o -o player
potato.o: potato.c potato.h
	gcc -Wall -std=gnu99 -c potato.c
clean:
	rm potato.o ringmaster player
