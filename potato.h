#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
int c_socket(int domain, int type, int protocol);
int c_bind(int sockfd, struct sockaddr *my_addr, int addrlen); 
int c_listen(int sockfd, int backlog);
ssize_t c_send(int sockfd, const void * msg, int len, int flags);
int c_recv(int sockfd, void *buf, int len, int flags);
int c_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
int c_connect(int sockfd, struct sockaddr *serv_addr, int addrlen);
int c_select(int numfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);
typedef struct _potato {
  int player;
  int hops;
}potato;

