#include "potato.h"
int port_num;
int num_players;
int num_hops;
int sockid;
void check_input (int argc, char **argv) {
  if (argc != 4) {
    printf("Invalid argument\n");
    exit(-1);
  }
  port_num = atoi(argv[1]);
  num_players = atoi(argv[2]);
  num_hops = atoi(argv[3]);
  if (num_players < 1) {
    printf("Must have at least one player\n");
    exit(-1);
  }
  if (num_hops < 0 || num_hops > 512) {
    printf("hop number must be in [0, 512]\n");
    exit(-1);
  }
  printf("Potato Ringmaster\nPlayers = %d\nHops = %d\n", num_players, num_hops);
}

void setup_master() {
  sockid = c_socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  struct sockaddr_in addrport;
  bzero(&addrport, sizeof(addrport));
  addrport.sin_family = AF_INET;
  addrport.sin_port = htons(port_num);
  addrport.sin_addr.s_addr = htonl(INADDR_ANY);
  c_bind(sockid, (struct sockaddr *) & addrport, sizeof(addrport));
  int option = 1;
  setsockopt(sockid, SOL_SOCKET, SO_REUSEADDR, (char *)&option, sizeof(option));  
  c_listen(sockid, num_players);
}




int main(int argc, char **argv) {
 
  check_input(argc, argv);  
  setup_master();
  
  struct sockaddr player[num_players];
  struct sockaddr_in playerhost[num_players];
  socklen_t len = sizeof(player[0]);
  int playersock[num_players];
  int player_num[2];
  player_num[1] = num_players;
  fd_set readfds;
  FD_ZERO(&readfds);
  int max = 0;
  
  for (int i = 0; i < num_players; i++) {
    playersock[i] = accept(sockid, (struct sockaddr *) & player[i], &len);
    printf("player %d is ready to play\n", i);
    if (playersock[i] > max) {
      max = playersock[i];
    }
    FD_SET(playersock[i], &readfds);
    player_num[0] = i;
    c_send(playersock[i], &player_num,  sizeof(player_num), 0);
    c_recv(playersock[i], &playerhost[i], sizeof(playerhost[i]), 0);
    if (i > 0) {
      c_send(playersock[i - 1], &playerhost[i], sizeof(playerhost[i]), 0);
    }
    if (i == num_players - 1) {
      c_send(playersock[i], &playerhost[0], sizeof(playerhost[0]), 0);
    }
  }
  // receive the player host address and send it to its neighbour
  char flag;
  char buf[512];
  int n;
  
  for (int i = 0; i < num_players; i++) {
    c_send(playersock[i], "initiate", 8, 0);
    n = c_recv(playersock[i], buf, 7, 0);
    if (n == 0) {
	printf("connect lost\n");
	exit(-1);
    }
    buf[n] = '\0';
    if (!strcmp("success", buf)){
      continue;
      } else {
      printf("failed to connect the ring\n");
      close(sockid);
      exit(-1);
    }

  }
  
  // send the start signal and receive the confirm
    
  srand((unsigned int)time(NULL));
  int first_player = rand()%(num_players);
  printf("Ready to start the game, sending potato to player %d\n", first_player);
  potato first_kick;
  first_kick.player = first_player;
  first_kick.hops = num_hops;
  c_send(playersock[first_player], &first_kick, sizeof(first_kick), 0);
  // send the first potato
  potato trace[num_hops + 1];
  potato sort_trace[num_hops + 1];
  int j = 0;
  flag = 1;
  fd_set temp = readfds;
  while(flag){
    readfds = temp;
    c_select(max + 1, &readfds, NULL, NULL, NULL);
    for (int i = 0; i < num_players; i++) {
      if (FD_ISSET(playersock[i], &readfds)) {
	c_recv(playersock[i], &trace[j], sizeof(potato), 0);
	sort_trace[trace[j].hops] = trace[j];
	j++;
	if (j > num_hops){
	  flag = 0;
	  break;
	}
      }
    }
  }
  for (int i = 0; i < num_players; i++) {
    close(playersock[i]);
  }
  close(sockid);
  printf("Trace of potato:\n");


  for (int i = num_hops; i >= 0; i--) {
    printf("%d", sort_trace[i].player);
    if (i != 0) {
      printf(", ");
    }else {
      printf("\n");
    }
  }
  
}
