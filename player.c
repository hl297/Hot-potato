#include "potato.h"
int sockid, hostid, clientid; 
void check_add(struct sockaddr * a, struct sockaddr * b) {
  printf("%s\n", a->sa_data);
  printf("%s\n", b->sa_data);
}
int my_max(int a, int b, int c) {
  int max;
  if (a > b) {
    max = a;
  } else {
    max = b;
  }
  if (c > max) {
    max = c;
  }
  return max;
}

void fill_addr(struct hostent * hp, char * machine_name, struct sockaddr_in * master, int port_num) {
  if (!hp) {
   printf("unknown host %s\n", machine_name);
   exit(-1);
  } else {
    master->sin_family = hp->h_addrtype;
    memcpy(&(master->sin_addr.s_addr), hp->h_addr, hp->h_length);
    master->sin_port = htons(port_num);
  }
}

int main(int argc, char **argv) {
  if (argc != 3) {
    printf("Invalid argument\n");
    return 0;
  }
  char * machine_name = argv[1];
  ssize_t port_num = atoi(argv[2]);
  hostid = c_socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  int neighbour[3];
  clientid = c_socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  neighbour[1] = clientid;
  struct sockaddr_in addrport;
  bzero(&addrport, sizeof(addrport));
  char hostname[128];
  gethostname(hostname,sizeof(hostname));
  struct hostent * hp = gethostbyname(hostname);
  if (!hp) {
    printf("unknown host %s\n", hostname);
    exit(-1);
  } else {
    addrport.sin_family = hp->h_addrtype;
    memcpy(&(addrport.sin_addr.s_addr), hp->h_addr, hp->h_length);
  }
  for (int i = 51015; i < 51097; i++) {
    addrport.sin_port = htons(i);
    if (bind(hostid, (struct sockaddr *) & addrport, sizeof(addrport)) == 0){
      break;
    }
    if (i == 51096) {
      printf("Can't not found a avaliable port\n");
      exit(-1);
    }
  }
 
  int sockid = c_socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  struct sockaddr_in master;
  bzero(&master, sizeof(master));
  hp = gethostbyname(machine_name);
  if (!hp) {
    printf("unknown host %s\n", machine_name);
    exit(-1);
  } else {
    master.sin_family = hp->h_addrtype;
    memcpy(&(master.sin_addr.s_addr), hp->h_addr, hp->h_length);
    master.sin_port = htons(port_num);
  }


  c_connect(sockid, (struct sockaddr *) & master, sizeof(master));
  // connect to master
  struct sockaddr_in rightaddr;
  struct sockaddr_in leftaddr;
  socklen_t left_len = sizeof(leftaddr);
  int playerid[2];
  char flag = 1;
  c_recv(sockid, playerid, sizeof(playerid), 0);
  printf("Connected as player %d out of %d total players\n", playerid[0], playerid[1]);
  c_listen(hostid, 5);
  // receive id and total number
  c_send(sockid, &addrport, sizeof(addrport), 0);
  // send the host port and addr
  c_recv(sockid, &rightaddr, sizeof(rightaddr), 0);
   // receive the host port and addr
 
  
  int n;
  char buf[512];
  n = c_recv(sockid, &buf, 8, 0);
  // receive the start signal
  if (n == 0) {
    printf("connect lost\n");
    exit(-1);
  }
  buf[n] = '\0';
  if(!strcmp("initiate", buf)){
    c_connect(neighbour[1], (struct sockaddr *) & rightaddr, sizeof(rightaddr));
  } else {
    printf("didn't get the right signal\n");
    exit(-1);
  }
  c_send(sockid, "success", 7, 0);
  neighbour[0] = accept(hostid, (struct sockaddr *) & leftaddr, &left_len);
  // connect the ring
  

  // send the confirm of ready
 
  neighbour[2] = sockid;
  srand((unsigned int) time(NULL) + playerid[0]);
  fd_set temp;
  fd_set readfds;
  FD_ZERO(&temp);
  FD_SET(neighbour[0], &temp);
  FD_SET(neighbour[1], &temp);
  FD_SET(neighbour[2], &temp);
  int max = my_max(neighbour[0], neighbour[1], neighbour[2]);
  potato ball;
  ball.player = 0;
  ball.hops = 0;
  flag = 1;
  int random;
  int next;
  int it = 0;
  while(flag) {
    readfds = temp;
    c_select(max + 1, &readfds, NULL, NULL, NULL);
    
    for(int i = 0; i < 3; i++) {
      if (FD_ISSET(neighbour[i], &readfds)) {
	flag = c_recv(neighbour[i], &ball, sizeof(ball), 0);
	if (flag == 0) break;
	
	ball.player = playerid[0];
	c_send(neighbour[2], &ball, sizeof(ball), 0);
	if (ball.hops == 0) {
	  it = 1;
	  break;
	}
	ball.hops -= 1;
	random = rand()%2;
	next = playerid[0] + random*2 - 1;
	if (next < 0) next = playerid[1] - 1;
	if (next > playerid[1] - 1) next = 0;
	c_send(neighbour[random], &ball, sizeof(ball), 0);
	printf("sending potato to %d\n", next);
	
	break;
      }
    }
  }
  if (it) {
    printf("I'm it\n");
  }
  close(neighbour[0]);
  close(neighbour[1]);
  close(sockid);
  close(hostid);
  
}                                                                   
  
