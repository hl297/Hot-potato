#include "potato.h"

int c_socket(int domain, int type, int protocol) {
  int sockid = socket(domain, type, protocol);
  if (sockid < 0) {
    perror("cant create a socket\n");
    close(sockid);
    exit(-1);
  }
  return sockid;
}
int c_bind(int sockfd, struct sockaddr *my_addr, int addrlen) {
 if (bind(sockfd, my_addr, addrlen) == -1) {
   perror("Cant bind to this port\n");
   close(sockfd);
   exit(-1);
 }
 return 0;
}
int c_listen(int sockfd, int backlog){
  if (listen(sockfd, backlog) != 0) {
    perror("Failed in listen\n");
    close(sockfd);
    exit(-1);
  }
  return 0;
}
ssize_t c_send(int sockfd, const void * msg, int len, int flags) {
  int bytesleft = len;
  int total = 0;
  int n;
  while (total < len) {
    n  = send(sockfd, msg + total, bytesleft, flags);
    if (n == -1) {
      perror("send failed\n");
      close(sockfd);
      exit(-1);
    }
    total += n;
    bytesleft -= n;
  }
  return n;
}


int c_recv(int sockfd, void *buf, int len, int flags) {
  int n = 0;
  while (n != len) {
    n = n + recv(sockfd, buf, len, flags);
    if (n == -1) {
      perror("receive failed\n");
      close(sockfd);
      exit(-1);
    } 
    if (n == 0) {
      break;
    }
  }
  return n;
}
int c_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen) {
  int playersock;
  if ((playersock = accept(sockfd, addr, addrlen)) < 0) {
    perror("Can't accpet the client\n");
    close(sockfd);
    exit(-1);
  }
  return playersock;
}

int c_connect(int sockfd, struct sockaddr *serv_addr, int addrlen) {
  if (connect(sockfd, serv_addr, addrlen) < 0) {
    perror("Can't connect to the host\n");
    close(sockfd);
    exit(-1);
  }
  return 0;
}
int c_select(int numfds, fd_set *readfds, fd_set *writefds,
           fd_set *exceptfds, struct timeval *timeout) {
  int i = select(numfds, readfds, writefds, exceptfds, timeout);
  if (i < 0) {
    perror("Select error\n");
    close(numfds);
    exit(-1);
  }
  return i;
}
